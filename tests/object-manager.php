<?php

declare(strict_types=1);

// @todo Symfony 5.4 update this file, see https://github.com/phpstan/phpstan-symfony

use App\Kernel;

require dirname(__DIR__).'/tests/bootstrap.php';
$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$kernel->boot();

return $kernel->getContainer()->get('doctrine')->getManager();
