DOCKER_COMPOSE_UP=docker-compose up --no-recreate --remove-orphans -d
DOCKER_COMPOSE_UP_RECREATE=docker-compose up --force-recreate --remove-orphans -d
DOCKER_COMPOSE_RUN=docker-compose run --rm

all: pre-configure configure build vendors webpack-build-dev start

restart: clean all

pre-configure:
	@echo "=> Checking docker command"         && command -v "docker" > /dev/null 2>&1            || (echo "You have to install the "docker" command" && false)
	@echo "=> Checking docker-compose command" && command -v "docker-compose" > /dev/null 2>&1    || (echo "You have to install the "docker-compose" command" && false)

configure:
	test -f docker-compose.override.yml || cp docker-compose.override.yml.dist docker-compose.override.yml
	test -f .env || cp .env.dist .env
ifeq ($(shell uname -s), Darwin)
	sed -i '' 's/#\(.*\)# Uncomment for MacOS/\1/g' docker-compose.override.yml
endif
	mkdir -p .cache/ssl .cache/xdebug/output
	test -f .cache/ssl/local.crt || (docker run --rm -v $$(pwd):/src alpine:3.9 sh -c "apk add openssl && openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /src/.cache/ssl/local.key -out /src/.cache/ssl/local.crt -subj \"/C=FR/ST=Paris/L=Paris/O=Oasis/CN=*.oasis.docker\" && cat /src/.cache/ssl/local.crt /src/.cache/ssl/local.key > /src/.cache/ssl/local.pem  && chown -R $$(id -u):$$(id -g) /src/.cache")

unconfigure:
	rm -f .env.local docker-compose.override.yml

#
# DOCKER
#

pull:
	docker-compose pull

build:
	docker-compose build --pull

build-prod:
	docker build -t zachee:latest -f docker/php-flex/Dockerfile .

start-db:
	$(DOCKER_COMPOSE_UP) traefik mysql adminer
	$(DOCKER_COMPOSE_RUN) wait -c mysql:3306 -t 60

start-php:
	$(DOCKER_COMPOSE_UP_RECREATE) traefik nginx fpm mailcatcher
	$(DOCKER_COMPOSE_RUN) wait -c nginx:80 -t 60
	@echo "\nStarted: \nhttp://oasis.local.cathocoulommiers.fr:8780\nhttps://oasis.local.cathocoulommiers.fr:8783\n(admin / pass)"

start: init-db start-php

stop:
	docker-compose stop

ps:
	docker-compose ps

logs:
	docker-compose logs -f --tail 10

clear-cache:
	rm -rf var/*

clean: clear-cache
	docker-compose down -v --remove-orphans || true
	rm -f helpme.log
	$(MAKE) -s unconfigure


#
# PROJECT
#

vendors:
	bin/tools composer install -n -v --profile --apcu-autoloader --prefer-dist --ignore-platform-reqs
	bin/node-tools yarn install --pure-lockfile

webpack-build-dev:
	bin/node-tools yarn encore dev

webpack-watch-dev:
	bin/node-tools yarn encore dev --watch

webpack-build-prod:
	bin/node-tools yarn encore production

init-db: start-db
	bin/tools rm -rf var/cache/*
	bin/tools bin/post-install-dev.sh

reset-db: start-db
	bin/tools bin/console doctrine:database:drop --if-exists --force
	$(MAKE) init-db

fix-cs-php:
	bin/tools vendor/bin/php-cs-fixer fix --verbose
	bin/tools vendor/bin/phpcbf

fix-cs: fix-cs-php
	bin/node-tools yarn run lint:fix
	bin/node-tools yarn run lint:css:fix

#
# TESTS
#

test: test-cs test-advanced

test-cs:
	bin/tools vendor/bin/php-cs-fixer fix --allow-risky yes --dry-run --verbose --diff
	bin/tools bin/console lint:twig templates --env=test
	bin/tools bin/console lint:yaml config --parse-tags --env=test
	bin/node-tools yarn run lint
	bin/node-tools yarn run lint:css

test-advanced:
	bin/tools sh -c "APP_DEBUG=1 APP_ENV=test bin/console c:w"
	bin/tools vendor/bin/phpstan analyse --configuration phpstan.neon --level max --no-progress .


#
# Help commands
#

helpme:
	@echo "Generating the helpme.log file..."
	$(MAKE) helpme-logs > helpme.log
	@echo "Done, you can send the 'helpme.log' file to your team :)"

helpme-logs:
	$(MAKE) -s pre-configure || true
	@echo "=========================="
	id || true
	@echo "=========================="
	git fetch -ap 2>&1 || true
	git status 2>&1 || true
	@echo "=========================="
	docker info 2>&1 || true
	@echo "=========================="
	docker-compose version 2>&1 || true
	@echo "=========================="
	(command -v "docker-machine" > /dev/null 2>&1 && docker-machine ls) || true
	@echo "=========================="
	(command -v "dinghy" > /dev/null 2>&1 && dinghy status) || true
	@echo "=========================="
	docker-compose config
	@echo "=========================="
	docker ps -a
	@echo "=========================="
	docker-compose ps
	@echo "=========================="
	docker-compose logs --no-color -t --tail=30
	@echo "=========================="
	cat var/log/dev.log | tail -n 200 || true
