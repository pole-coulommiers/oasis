/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// sort is super fast, even with huge tables, so that is probably not the issue
// Not solved with documentFragment, same issue... :(
// My guess is that it is simply too much to hold in memory, since
// it freezes even before sortable is called if the table is too big in index.html

document.addEventListener('click', (e) => {
  const downClass = ' dir-d ';
  const upClass = ' dir-u ';
  const regexDir = / dir-(u|d) /;
  const regexTable = /\bsortable\b/;
  const element = e.target;

  function reClassify(elem, dir) {
    // eslint-disable-next-line no-param-reassign
    elem.className = elem.className.replace(regexDir, '') + dir;
  }

  function getValue(elem) {
    return elem.getAttribute('data-sort') || elem.innerText;
  }

  if (element.nodeName === 'TH') {
    try {
      if (element.classList.contains('no-sort')) {
        return;
      }

      const tr = element.parentNode;
      // var table = element.offsetParent; // Fails with positioned table elements
      // this is the only way to make really, really sure. A few more bytes though... 😡
      const table = tr.parentNode.parentNode;
      if (regexTable.test(table.className)) {
        let columnIndex;
        const nodes = tr.cells;

        // reset thead cells and get column index
        for (let i = 0; i < nodes.length; i++) {
          if (nodes[i] === element) {
            columnIndex = i;
          } else {
            reClassify(nodes[i], '');
          }
        }

        let dir = downClass;

        // check if we're sorting up or down, and update the css accordingly
        if (element.className.indexOf(downClass) !== -1) {
          dir = upClass;
        }

        reClassify(element, dir);

        // extract all table rows, so the sorting can start.
        const orgTbody = table.tBodies[0];

        // get the array rows in an array, so we can sort them...
        const rows = [].slice.call(orgTbody.rows, 0);

        const reverse = dir === upClass;

        // sort them using custom built in array sort.
        rows.sort((a, b) => {
          const x = getValue((reverse ? a : b).cells[columnIndex]);
          const y = getValue((reverse ? b : a).cells[columnIndex]);
          // var y = (reverse ? b : a).cells[columnIndex].innerText
          // var x = (reverse ? a : b).cells[columnIndex].innerText
          return Number.isNaN(x - y) ? x.localeCompare(y) : x - y;
        });

        // Make a clone without content
        const cloneTbody = orgTbody.cloneNode();

        // Build a sorted table body and replace the old one.
        while (rows.length) {
          cloneTbody.appendChild(rows.splice(0, 1)[0]);
        }

        // And finally insert the end result
        table.replaceChild(cloneTbody, orgTbody);
      }
    } catch (error) {
      // console.log(error);
    }
  }
});
