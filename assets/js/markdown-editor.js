/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import ClassicEditor from '@ckeditor/ckeditor5-build-classic/build/ckeditor';

if (document.querySelector('.markdown-editor')) {
  ClassicEditor
    .create(document.querySelector('.markdown-editor'), {
      toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
      heading: {
        options: [
          { model: 'paragraph', title: 'Paragraphe', class: 'ck-heading_paragraph' },
          {
            model: 'heading3', view: 'h3', title: 'Titre 1', class: 'ck-heading_heading3',
          },
          {
            model: 'heading4', view: 'h4', title: 'Titre 2', class: 'ck-heading_heading4',
          },
          {
            model: 'heading5', view: 'h5', title: 'Titre 3', class: 'ck-heading_heading5',
          },
          {
            model: 'heading6', view: 'h6', title: 'Titre 4', class: 'ck-heading_heading6',
          },
        ],
      },
      language: 'fr',
    });
  /* .catch((error) => {
       console.log(error);
    }); */
}
