/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import 'bootstrap';

// start the Stimulus application
// import './bootstrap';

// Our scripts
import './sortable';
import './markdown-editor';
