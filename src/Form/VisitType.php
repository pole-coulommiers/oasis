<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\Package;
use App\Entity\Person;
use App\Entity\Visit;
use App\Repository\PersonRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VisitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('breakfast', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('administrative', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('locker', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('washing', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('clothes', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('listen', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('shower', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('hairdressing', null, [
                'label_attr' => [
                    'class' => 'checkbox-switch',
                ],
            ])
            ->add('package', EnumType::class, [
                'class' => Package::class,
                'choice_label' => static function (\UnitEnum $choice): string {
                    return 'package.'.$choice->name;
                },
            ])
            ->add('observations', TextareaType::class, [
                'attr' => ['class' => 'markdown-editor'],
                'required' => false,
                'label_attr' => [
                    'class' => 'h2',
                ],
            ])
            ->add('who')
            ->add('datetime', DateTimeType::class)
        ;

        if (false === $options['has-person']) {
            $builder->add('person', EntityType::class, [
                'class' => Person::class,
                'query_builder' => function (PersonRepository $personRepository): QueryBuilder {
                    return $personRepository->createQueryBuilder('p')
                        ->orderBy('p.lastname', 'ASC')
                        ->addOrderBy('p.firstname', 'ASC');
                },
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Visit::class,
            'has-person' => false,
        ]);
    }
}
