<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('email')
            ->add('addressLine1')
            ->add('addressLine2')
            ->add('city')
            ->add('postalCode')
            ->add('sender')
            ->add('warning')
            ->add('notes', TextareaType::class, [
                'attr' => ['class' => 'markdown-editor'],
                'required' => false,
            ])
            ->add('rules', FileType::class, [
                'label' => 'Rules (PDF file)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '8192K',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
