<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Visit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Visit>
 *
 * @method Visit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Visit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Visit[]    findAll()
 * @method Visit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VisitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Visit::class);
    }

    /**
     * @return Visit[]
     */
    public function findByPeriod(\DateTimeInterface $fromDate, \DateTimeInterface $toDate): array
    {
        return $this->createQueryBuilder('v')
            ->where('v.datetime >= :fromDate')
            ->andWhere('v.datetime <= :toDate')
            ->setParameter('fromDate', $fromDate)
            ->setParameter('toDate', $toDate)
            ->orderBy('v.datetime', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getStats(\DateTimeInterface $fromDate, \DateTimeInterface $toDate): mixed
    {
        return $this->createQueryBuilder('v')
            ->where('v.datetime >= :fromDate')
            ->andWhere('v.datetime <= :toDate')
            ->select('COUNT(v.id) as visitSum')
            ->addSelect('COUNT(distinct v.person) as personSum')
            ->addSelect('SUM(v.breakfast) as breakfastSum')
            ->addSelect('SUM(v.administrative) as administrativeSum')
            ->addSelect('SUM(v.locker) as lockerSum')
            ->addSelect('SUM(v.washing) as washingSum')
            ->addSelect('SUM(v.clothes) as clothesSum')
            ->addSelect('SUM(v.listen) as listenSum')
            ->addSelect('SUM(v.shower) as showerSum')
            ->addSelect('SUM(v.hairdressing) as hairdressingSum')
            ->setParameter('fromDate', $fromDate)
            ->setParameter('toDate', $toDate)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
