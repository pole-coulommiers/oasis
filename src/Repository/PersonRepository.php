<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Person>
 *
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * @return Person[]
     */
    public function findBySearch(string $search): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.firstname LIKE :search')
            ->orWhere('p.lastname LIKE :search')
            ->orWhere('p.id LIKE :search')
            ->orderBy('p.lastname', 'ASC')
            ->addOrderBy('p.firstname', 'ASC')
            ->setParameter('search', '%'.$search.'%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Person[]
     */
    public function findAllWithVisits(): array
    {
        return $this->createQueryBuilder('p')
            ->select(['p', 'v'])
            ->leftJoin('p.visits', 'v')
            ->orderBy('p.lastname', 'ASC')
            ->addOrderBy('p.firstname', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
