<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Twig;

use App\Entity\User;
use App\Services\GrantedService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private GrantedService $grantedService;

    public function __construct(GrantedService $grantedService)
    {
        $this->grantedService = $grantedService;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('userIsGranted', [$this, 'userIsGranted']),
        ];
    }

    public function userIsGranted(User $user, string $attribute, mixed $subject = null): bool
    {
        return $this->grantedService->isGranted($user, $attribute, $subject);
    }
}
