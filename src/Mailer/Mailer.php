<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Mailer;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class Mailer
{
    protected MailerInterface $mailer;
    protected UrlGeneratorInterface $router;
    protected Environment $templating;
    protected ParameterBagInterface $parameters;

    public function __construct(MailerInterface $mailer, UrlGeneratorInterface $router, Environment $templating, ParameterBagInterface $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    public function sendRegistration(User $user): void
    {
        $url = $this->router->generate(
            'user_activate',
            [
                'token' => $user->getToken(),
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $message = (new TemplatedEmail())
            ->subject('[L’Oasis] Bienvenue '.$user->getFirstname().' '.$user->getLastname())
            ->from($this->getFrom())
            ->to($user->getEmail())
            ->htmlTemplate('emails/activation.html.twig')
            ->context([
                'user' => $user,
                'confirmation_url' => $url,
            ])
        ;
        $message->getHeaders()
            // this header tells auto-repliers ("email holiday mode") to not
            // reply to this message because it's an automated email
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }

    public function sendResetPassword(User $user): void
    {
        $message = (new TemplatedEmail())
            ->subject('[L’Oasis] Réinitialisation de votre mot de passe')
            ->from($this->getFrom())
            ->to($user->getEmail())
            ->htmlTemplate('emails/reset_password.html.twig')
            ->context([
                'name' => $user->getUserIdentifier(),
                'user' => $user,
                'token' => $user->getToken(),
            ])
        ;
        $message->getHeaders()
            ->addTextHeader('X-Auto-Response-Suppress', 'OOF, DR, RN, NRN, AutoReply')
        ;

        $this->mailer->send($message);
    }

    protected function getFrom(): Address
    {
        return new Address($this->parameters->get('configuration.from_email'), $this->parameters->get('configuration.name'));
    }
}
