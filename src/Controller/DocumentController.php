<?php
/**
 * Copyright 2022 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Document;
use App\Entity\Person;
use App\Form\DocumentType;
use App\Repository\DocumentRepository;
use App\Services\DocumentsUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/document')]
class DocumentController extends AbstractController
{
    #[Route('/new/{id}', name: 'document_new', methods: ['GET', 'POST'])]
    public function new(Request $request, DocumentRepository $documentRepository, Person $person, DocumentsUploader $documentsUploader): Response
    {
        $document = new Document();
        $document->setPerson($person);
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $docfile */
            $docfile = $form->get('filename')->getData();
            if (null !== $docfile) {
                $docFileName = $documentsUploader->upload($docfile);
                if (null !== $docFileName) {
                    $document->setFilename($docFileName);

                    $documentRepository->add($document, true);
                    $this->addFlash('success', 'Le document a bien été ajouté');

                    return $this->redirectToRoute('person_show', ['id' => $person->getId()], Response::HTTP_SEE_OTHER);
                }
            }
            $this->addFlash('error', 'Le fichier n’a pas pu être sauvegardé');
        }

        return $this->render('document/new.html.twig', [
            'document' => $document,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', name: 'document_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Document $document, DocumentRepository $documentRepository, DocumentsUploader $documentsUploader, Filesystem $filesystem): Response
    {
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $docfile */
            $docfile = $form->get('filename')->getData();
            if (null !== $docfile) {
                $oldFileName = $document->getFilename();
                $docFileName = $documentsUploader->upload($docfile);
                if (null !== $docFileName) {
                    $document->setFilename($docFileName);
                    $filesystem->remove($this->getParameter('documents_directory').'/'.$oldFileName);
                } else {
                    $this->addFlash('error', 'Le fichier n’a pas pu être sauvegardé');
                }
            }

            $documentRepository->add($document, true);
            $this->addFlash('success', 'Le document a bien été modifié');

            return $this->redirectToRoute('person_show', ['id' => $document->getPerson()->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('document/edit.html.twig', [
            'document' => $document,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'document_delete', methods: ['POST'])]
    public function delete(Request $request, Document $document, DocumentRepository $documentRepository, Filesystem $filesystem): Response
    {
        /** @var string $token */
        $token = $request->request->get('_token');
        if ($this->isCsrfTokenValid('delete'.$document->getId(), $token)) {
            $filesystem->remove($this->getParameter('documents_directory').'/'.$document->getFilename());
            $documentRepository->remove($document, true);
        }

        $this->addFlash('success', 'Le document a bien été supprimé');

        return $this->redirectToRoute('person_show', ['id' => $document->getPerson()->getId()], Response::HTTP_SEE_OTHER);
    }
}
