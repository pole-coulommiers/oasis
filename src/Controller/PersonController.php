<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use App\Services\RulesUploader;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/person')]
class PersonController extends AbstractController
{
    #[Route('/', name: 'person_index', methods: ['GET'])]
    public function index(PersonRepository $personRepository): Response
    {
        return $this->render('person/index.html.twig', [
            'people' => $personRepository->findAllWithVisits(),
        ]);
    }

    #[Route('/new', name: 'person_new', methods: ['GET', 'POST'])]
    public function new(ManagerRegistry $doctrine, Request $request, RulesUploader $rulesUploader): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $rulesfile */
            $rulesfile = $form->get('rules')->getData();
            if (null !== $rulesfile) {
                $rulesFileName = $rulesUploader->upload($rulesfile);
                if (null !== $rulesFileName) {
                    $person->setRules($rulesFileName);
                } else {
                    $this->addFlash('error', 'Le fichier du règlement intérieur signé n’a pas pu être sauvegardé');
                }
            }

            $entityManager = $doctrine->getManager();
            $entityManager->persist($person);
            $entityManager->flush();

            $this->addFlash('success', 'Le bénéficiaire a bien été ajouté');

            return $this->redirectToRoute('person_show', ['id' => $person->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('person/new.html.twig', [
            'person' => $person,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'person_show', methods: ['GET'])]
    public function show(Person $person): Response
    {
        return $this->render('person/show.html.twig', [
            'person' => $person,
        ]);
    }

    #[Route('/{id}/edit', name: 'person_edit', methods: ['GET', 'POST'])]
    public function edit(ManagerRegistry $doctrine, Request $request, Person $person, RulesUploader $rulesUploader, Filesystem $filesystem): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile|null $rulesfile */
            $rulesfile = $form->get('rules')->getData();
            if (null !== $rulesfile) {
                $oldFileName = $person->getRules();
                $rulesFileName = $rulesUploader->upload($rulesfile);
                if (null !== $rulesFileName) {
                    $person->setRules($rulesFileName);
                    $filesystem->remove($this->getParameter('rules_directory').'/'.$oldFileName);
                } else {
                    $this->addFlash('error', 'Le fichier du règlement intérieur signé n’a pas pu être sauvegardé');
                }
            }

            $doctrine->getManager()->flush();

            $this->addFlash('success', 'Le bénéficiaire a bien été mis à jour');

            return $this->redirectToRoute('person_show', ['id' => $person->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('person/edit.html.twig', [
            'person' => $person,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'person_delete', methods: ['POST'])]
    public function delete(ManagerRegistry $doctrine, Request $request, Person $person, Filesystem $filesystem): Response
    {
        /** @var string $token */
        $token = $request->request->get('_token');
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $token)) {
            $oldFileName = $person->getRules();
            $entityManager = $doctrine->getManager();
            $entityManager->remove($person);
            $entityManager->flush();

            if (null !== $oldFileName) {
                $filesystem->remove($this->getParameter('rules_directory').'/'.$oldFileName);
            }

            $this->addFlash('success', 'Le bénéficiaire a bien été supprimé');
        }

        return $this->redirectToRoute('person_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/', name: 'person_search', methods: ['POST'])]
    public function search(Request $request, PersonRepository $personRepository): Response
    {
        /** @var string $search */
        $search = $request->get('search');

        return $this->render('person/search.html.twig', [
            'search' => $search,
            'people' => $personRepository->findBySearch($search),
        ]);
    }
}
