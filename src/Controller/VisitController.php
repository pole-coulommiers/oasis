<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\Person;
use App\Entity\Visit;
use App\Form\PeriodType;
use App\Form\VisitType;
use App\Repository\VisitRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/visit')]
class VisitController extends AbstractController
{
    use UserableTrait;

    #[Route('/', name: 'visit_index', methods: ['GET', 'POST'])]
    public function index(VisitRepository $visitRepository, Request $request): Response
    {
        $from = new \DateTime(date('Y').'-01-01');
        $to = new \DateTime('now');
        $form = $this->createForm(PeriodType::class, ['from' => $from, 'to' => $to]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \DateTime $from */
            $from = $form->get('from')->getData();
            /** @var \DateTime $to */
            $to = $form->get('to')->getData();
            $to = $to->add(new \DateInterval('P1D'));

            $this->addFlash('notice', 'Les résultats sont filtrés');
        }

        return $this->render('visit/index.html.twig', [
            'visits' => $visitRepository->findByPeriod($from, $to),
            'form' => $form,
        ]);
    }

    #[Route('/stats', name: 'visit_stats', methods: ['GET', 'POST'])]
    public function stats(VisitRepository $visitRepository, Request $request): Response
    {
        $from = new \DateTime(date('Y').'-01-01');
        $to = new \DateTime('now');
        $form = $this->createForm(PeriodType::class, ['from' => $from, 'to' => $to]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \DateTime $from */
            $from = $form->get('from')->getData();
            /** @var \DateTime $to */
            $to = $form->get('to')->getData();
            $to = $to->add(new \DateInterval('P1D'));

            $this->addFlash('notice', 'Les résultats sont filtrés');
        }

        $packages = [
            'cold' => 0,
            'hot' => 0,
            'undefined' => 0,
        ];
        $dates = [];
        $visits = $visitRepository->findByPeriod($from, $to);
        foreach ($visits as $visit) {
            switch ($visit->getPackage()->value) {
                case 'COLD':
                    $packages['cold']++;
                    break;
                case 'HOT':
                    $packages['hot']++;
                    break;
                case 'UNDEFINED':
                    $packages['undefined']++;
                    break;
            }

            if (null !== $visit->getDatetime()) {
                $date = $visit->getDatetime()->format('Y-m-d');
                if (isset($visits[$date])) {
                    ++$dates[$date];
                } else {
                    $dates[$date] = 0;
                }
            }
        }

        return $this->render('visit/stats.html.twig', [
            'stats' => $visitRepository->getStats($from, $to),
            'form' => $form,
            'from' => $from,
            'to' => $to,
            'packages' => $packages,
            'days' => count($dates),
        ]);
    }

    #[Route('/new/{id?}', name: 'visit_new', methods: ['GET', 'POST'])]
    public function new(ManagerRegistry $doctrine, Request $request, ?Person $person): Response
    {
        $visit = new Visit();
        $visit->setDatetime(new \DateTime())
            ->setWho($this->getAppUser()->getFirstname().' '.$this->getAppUser()->getLastname())
        ;
        $hasPerson = null !== $person;
        if ($hasPerson) {
            $visit->setPerson($person);
        }
        $form = $this->createForm(VisitType::class, $visit, [
            'has-person' => $hasPerson,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            $entityManager->persist($visit);
            $entityManager->flush();

            $this->addFlash('success', 'La visite a bien été saisie');

            return $this->redirectToRoute('visit_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('visit/new.html.twig', [
            'visit' => $visit,
            'form' => $form,
            'person' => $person,
        ]);
    }

    #[Route('/{id}', name: 'visit_show', methods: ['GET'])]
    public function show(Visit $visit): Response
    {
        return $this->render('visit/show.html.twig', [
            'visit' => $visit,
        ]);
    }

    #[Route('/{id}/edit', name: 'visit_edit', methods: ['GET', 'POST'])]
    public function edit(ManagerRegistry $doctrine, Request $request, Visit $visit): Response
    {
        $form = $this->createForm(VisitType::class, $visit, [
            'has-person' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $doctrine->getManager()->flush();

            $this->addFlash('success', 'La visite a bien été modifiée');

            return $this->redirectToRoute('visit_show', ['id' => $visit->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('visit/edit.html.twig', [
            'visit' => $visit,
            'form' => $form,
            'person' => $visit->getPerson(),
        ]);
    }

    #[Route('/{id}', name: 'visit_delete', methods: ['POST'])]
    public function delete(ManagerRegistry $doctrine, Request $request, Visit $visit): Response
    {
        /** @var ?string $token */
        $token = $request->request->get('_token');
        if ($this->isCsrfTokenValid('delete'.$visit->getId(), $token)) {
            $entityManager = $doctrine->getManager();
            $entityManager->remove($visit);
            $entityManager->flush();

            $this->addFlash('success', 'La visite a bien été supprimée');
        }

        return $this->redirectToRoute('visit_index', [], Response::HTTP_SEE_OTHER);
    }
}
