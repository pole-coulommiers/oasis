<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Repository\PersonRepository;
use App\Repository\VisitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(PersonRepository $personRepository, VisitRepository $visitRepository): Response
    {
        $name = 'World';

        return $this->render('home/home.html.twig', [
            'name' => $name,
            'people' => $personRepository->findBy([], ['createdAt' => 'DESC'], 5),
            'visits' => $visitRepository->findBy([], ['datetime' => 'DESC'], 5),
        ]);
    }
}
