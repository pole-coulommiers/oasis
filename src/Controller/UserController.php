<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use App\Controller\Traits\UserableTrait;
use App\Entity\User;
use App\Form\ForgotType;
use App\Form\ProfilType;
use App\Form\ResetPasswordType;
use App\Form\UserType;
use App\Mailer\Mailer;
use App\Repository\UserRepository;
use App\Services\GrantedService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/user')]
class UserController extends AbstractController
{
    use UserableTrait;

    #[Route('/profil', name: 'user_profil', methods: ['GET', 'POST'])]
    public function profil(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $form = $this->createForm(ProfilType::class, $this->getAppUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $form->get('new_password')->getData()) {
                /** @var string $newPassword */
                $newPassword = $form->get('new_password')->getData();
                $this->getAppUser()->setPassword($passwordHasher->hashPassword(
                    $this->getAppUser(),
                    $newPassword
                ));
            }
            $doctrine->getManager()->flush();
            $this->addFlash('success', 'Votre profil a bien été mis à jour');
        }

        return $this->render('user/profil.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/activate', name: 'user_activate', methods: ['GET', 'POST'])]
    public function activate(ManagerRegistry $doctrine, Request $request, UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher): Response
    {
        $token = $request->get('token');
        $user = $userRepository->findOneBy(['token' => $token]);
        if (null === $user) {
            throw $this->createNotFoundException('L’utilisateur n’existe pas');
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var string $plainPassword */
            $plainPassword = $form->get('plainPassword')->getData();
            $user->setPassword($passwordHasher->hashPassword(
                $user,
                $plainPassword
            ));
            $user->setToken(null)
                ->setEnabled(true)
            ;

            $doctrine->getManager()->flush();
            $this->addFlash('success', 'Félicitations, votre compte est maintenant activé');

            return $this->redirectToRoute('login', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('home/activate.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/forgot', name: 'user_forgot', methods: ['GET', 'POST'])]
    public function forgot(ManagerRegistry $doctrine, Request $request, UserRepository $userRepository, Mailer $mailer): Response
    {
        $form = $this->createForm(ForgotType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy([
                'username' => $form->get('username')->getData(),
                'email' => $form->get('email')->getData(),
            ]);

            if (null !== $user) {
                $token = md5(random_bytes(20));
                $user->setToken($token);

                $entityManager = $doctrine->getManager();
                $entityManager->flush();

                try {
                    $mailer->sendResetPassword($user);
                    $this->addFlash('success', 'Un email vous a été adressé pour changer votre mot de passe');

                    return $this->redirectToRoute('login');
                } catch (TransportExceptionInterface $e) {
                    $this->addFlash('warning', 'Impossible d’envoyer le mail de confirmation, contacter un administrateur');
                }
            } else {
                $this->addFlash('error', 'Cet utilisateur est introuvable, vérifier l’email et le nom d’utilisateur');
            }
        }

        return $this->render('home/forgot.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/reset', name: 'user_reset', methods: ['GET', 'POST'])]
    public function reset(ManagerRegistry $doctrine, Request $request, UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher): Response
    {
        $username = $request->get('username');
        $token = $request->get('token');
        $user = $userRepository->findOneBy(['username' => $username, 'token' => $token]);
        if (null === $user) {
            throw $this->createNotFoundException('Le lien de réinitialisation du mot de passe est périmé !');
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var string $plainPassword */
            $plainPassword = $form->get('plainPassword')->getData();
            $user->setPassword($passwordHasher->hashPassword(
                $user,
                $plainPassword
            ));
            $user->setToken(null)
                ->setEnabled(true)
            ;

            $doctrine->getManager()->flush();
            $this->addFlash('success', 'Votre mot de passe a été modifié avec succès');

            return $this->redirectToRoute('login', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('home/activate.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/', name: 'user_index', methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'user_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function new(ManagerRegistry $doctrine, Request $request, Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $doctrine->getManager();
            if (true === $form->get('is_admin')->getData()) {
                $user->setRoles(['ROLE_ADMIN']);
            }
            $user->setEnabled(false)
                ->setToken(md5(random_bytes(20)))
            ;
            $entityManager->persist($user);
            $entityManager->flush();

            $mailer->sendRegistration($user);

            $this->addFlash('success', 'L’utilisateur a bien été ajouté, il va recevoir un mail pour valider son compte et choisir son mot de passe');

            return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/edit', name: 'user_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(ManagerRegistry $doctrine, Request $request, User $user, GrantedService $grantedService): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var bool $is_admin */
            $is_admin = $form->get('is_admin')->getData();
            if (!$is_admin && $user === $this->getAppUser()) {
                $this->addFlash('error', 'Vous ne pouvez pas vous retirer les droits d’admnistrateur');
            } else {
                if ($is_admin) {
                    $user->setRoles(['ROLE_ADMIN']);
                } else {
                    $user->setRoles(['ROLE_USER']);
                }
                $doctrine->getManager()->flush();
                $this->addFlash('success', 'L’utilisateur a bien été modifié');

                return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'user_delete', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(ManagerRegistry $doctrine, Request $request, User $user): Response
    {
        /** @var string|null $token */
        $token = $request->request->get('_token');
        if ($user === $this->getAppUser()) {
            $this->addFlash('error', 'Vous ne pouvez pas vous supprimer !');
        } elseif ($this->isCsrfTokenValid('delete'.$user->getId(), $token)) {
            $entityManager = $doctrine->getManager();
            $entityManager->remove($user);
            $this->addFlash('success', 'L’utilisateur a bien été supprimé');
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
    }
}
