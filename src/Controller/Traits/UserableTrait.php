<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Traits;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

trait UserableTrait
{
    /**
     * @return object|UserInterface|null
     */
    abstract protected function getUser();

    protected function getAppUser(): User
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new \RuntimeException('Invalid user entity');
        }

        return $user;
    }
}
