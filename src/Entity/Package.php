<?php
/**
 * Copyright 2022 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

enum Package: string
{
    case Not = 'NOT';
    case Undefined = 'UNDEFINED';
    case Cold = 'COLD';
    case Hot = 'HOT';
}
