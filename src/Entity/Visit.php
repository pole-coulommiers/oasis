<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\VisitRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VisitRepository::class)]
class Visit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Person::class, fetch: 'EAGER', inversedBy: 'visits')]
    #[ORM\JoinColumn(nullable: false)]
    private Person $person;

    #[ORM\Column(type: 'boolean')]
    private bool $breakfast;

    #[ORM\Column(type: 'boolean')]
    private bool $administrative;

    #[ORM\Column(type: 'boolean')]
    private bool $locker;

    #[ORM\Column(type: 'boolean')]
    private bool $washing;

    #[ORM\Column(type: 'boolean')]
    private bool $clothes;

    #[ORM\Column(type: 'boolean')]
    private bool $listen;

    #[ORM\Column(type: 'boolean')]
    private bool $shower;

    #[ORM\Column(type: 'boolean')]
    private bool $hairdressing;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $observations;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $who;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $datetime;

    #[ORM\Column(type: 'package', length: 255)]
    private Package $package = Package::Not;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getBreakfast(): ?bool
    {
        return $this->breakfast;
    }

    public function setBreakfast(bool $breakfast): self
    {
        $this->breakfast = $breakfast;

        return $this;
    }

    public function getAdministrative(): ?bool
    {
        return $this->administrative;
    }

    public function setAdministrative(bool $administrative): self
    {
        $this->administrative = $administrative;

        return $this;
    }

    public function getLocker(): ?bool
    {
        return $this->locker;
    }

    public function setLocker(bool $locker): self
    {
        $this->locker = $locker;

        return $this;
    }

    public function getWashing(): ?bool
    {
        return $this->washing;
    }

    public function setWashing(bool $washing): self
    {
        $this->washing = $washing;

        return $this;
    }

    public function getClothes(): ?bool
    {
        return $this->clothes;
    }

    public function setClothes(bool $clothes): self
    {
        $this->clothes = $clothes;

        return $this;
    }

    public function getListen(): ?bool
    {
        return $this->listen;
    }

    public function setListen(bool $listen): self
    {
        $this->listen = $listen;

        return $this;
    }

    public function getShower(): ?bool
    {
        return $this->shower;
    }

    public function setShower(bool $shower): self
    {
        $this->shower = $shower;

        return $this;
    }

    public function getHairdressing(): ?bool
    {
        return $this->hairdressing;
    }

    public function setHairdressing(bool $hairdressing): self
    {
        $this->hairdressing = $hairdressing;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): self
    {
        $this->observations = $observations;

        return $this;
    }

    public function getWho(): ?string
    {
        return $this->who;
    }

    public function setWho(string $who): self
    {
        $this->who = $who;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getPackage(): Package
    {
        return $this->package;
    }

    public function setPackage(Package $package): self
    {
        $this->package = $package;

        return $this;
    }

    public function isBreakfast(): ?bool
    {
        return $this->breakfast;
    }

    public function isAdministrative(): ?bool
    {
        return $this->administrative;
    }

    public function isLocker(): ?bool
    {
        return $this->locker;
    }

    public function isWashing(): ?bool
    {
        return $this->washing;
    }

    public function isClothes(): ?bool
    {
        return $this->clothes;
    }

    public function isListen(): ?bool
    {
        return $this->listen;
    }

    public function isShower(): ?bool
    {
        return $this->shower;
    }

    public function isHairdressing(): ?bool
    {
        return $this->hairdressing;
    }
}
