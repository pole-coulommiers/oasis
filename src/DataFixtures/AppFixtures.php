<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\DataFixtures;

use App\Entity\Title;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $monsieur = new Title();
        $monsieur->setSex('M')
            ->setName('Monsieur')
        ;
        $manager->persist($monsieur);
        $titles = [
            ['sex' => 'F', 'name' => 'Madame'],
            ['sex' => 'F', 'name' => 'Mademoiselle'],
            ['sex' => 'M', 'name' => 'Frère'],
            ['sex' => 'F', 'name' => 'Sœur'],
            ['sex' => 'M', 'name' => 'Père'],
            ['sex' => 'M', 'name' => 'M. l’abbé'],
            ['sex' => 'M', 'name' => 'Monseigneur'],
        ];
        array_walk($titles, static function ($titleData, $key, $manager): void {
            $title = new Title();
            $title->setSex($titleData['sex'])
                ->setName($titleData['name'])
            ;
            $manager->persist($title);
        }, $manager);

        $admin = new User();
        $admin->setEmail('admin@oasis.fr')
            ->setUsername('admin')
            ->setTitle($monsieur)
            ->setFirstname('Oasis')
            ->setLastname('Admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setPassword($this->encoder->hashPassword($admin, 'pass'))
            ->setEnabled(true)
        ;
        $manager->persist($admin);
        $user = new User();
        $user->setEmail('user@oasis.fr')
            ->setUsername('user')
            ->setTitle($monsieur)
            ->setFirstname('Oasis')
            ->setLastname('User')
            ->setRoles(['ROLE_USER'])
            ->setEnabled(false)
        ;
        $manager->persist($user);

        $manager->flush();
    }
}
