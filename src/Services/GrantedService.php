<?php
/**
 * Copyright 2021 Grégoire Oliveira Silva <gregoire@theo-net.org>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Services;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class GrantedService
{
    private AccessDecisionManagerInterface $accessDecisionManager;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    /** @phpstan-ignore-next-line */
    public function isGranted(User $user, $attributes, $object = null): bool
    {
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
        $token = new UsernamePasswordToken($user, 'none', $user->getRoles());

        return $this->accessDecisionManager->decide($token, $attributes, $object);
    }
}
