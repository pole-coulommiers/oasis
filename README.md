Extranet de l’Oasis
===================

Installation de l’environnement
-------------------------------

        yarn set version classic
        composer install
        yarn install

Développement
-------------

Watcher pour les assets

        yarn run watch

À exécuter avant chaque commit

        make fix-cs
        make test

Utilisateur par défaut
----------------------

Les identifiants de l’admnistrateur par défaut sont : 

| Nom d’utilisateur | Mot de passe |
|-------------------|--------------|
| admin             | pass         |


Déploiement
-----------

Pour déployer l'application : 

        composer install --no-dev --optimize-autoloader
        composer dump-env prod
        APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
        yarn install --pure-lockfile
        yarn encore production
        php bin/console doctrine:migrations:migrate

Mais il faudra charger la base de données vide, pour cela exécuteé plutôt : 

        composer install --optimize-autoloader
        APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
        yarn install --pure-lockfile
        yarn encore production
        php bin/console doctrine:migrations:migrate
        php bin/console doctrine:fixture:load
        rm -rf vendor
        composer install --no-dev --optimize-autoloader
        composer dump-env prod


Restaurer base de données
-------------------------

        mysql mysql -u oasisuser --password=oasispwd oasisdb --default-character-set=utf8mb4  < ./oasis.sql