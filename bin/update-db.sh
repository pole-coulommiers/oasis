#!/usr/bin/env sh
set -ex

bin/console doctrine:mapping:info
bin/console doctrine:migrations:migrate -n
