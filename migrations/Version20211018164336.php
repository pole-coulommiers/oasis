<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211018164336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add updated at and created at';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person DROP created_at, DROP updated_at');
    }
}
