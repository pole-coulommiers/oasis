<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220603122454 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add rules field to Person';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person ADD rules VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person DROP rules');
    }
}
