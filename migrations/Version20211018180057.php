<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211018180057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add visits';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE visit (id INT AUTO_INCREMENT NOT NULL, person_id INT NOT NULL, breakfast TINYINT(1) NOT NULL, administrative TINYINT(1) NOT NULL, locker TINYINT(1) NOT NULL, washing TINYINT(1) NOT NULL, clothes TINYINT(1) NOT NULL, listen TINYINT(1) NOT NULL, shower TINYINT(1) NOT NULL, observations LONGTEXT DEFAULT NULL, who VARCHAR(255) DEFAULT NULL, datetime DATETIME NOT NULL, INDEX IDX_437EE939217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE939217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE visit');
    }
}
