<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220604111018 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add cold/hot/undefined to visit.package';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE visit ADD package_new VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE visit SET package_new = CAST(package AS CHAR)');
        $this->addSql('ALTER TABLE visit DROP COLUMN package');
        $this->addSql('ALTER TABLE visit CHANGE COLUMN package_new package VARCHAR(255)');
        $this->addSql('UPDATE visit SET package = "NOT" WHERE package = "0"');
        $this->addSql('UPDATE visit SET package = "UNDEFINED" WHERE package = "1"');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE visit SET package = "1" WHERE package != "NOT"');
        $this->addSql('UPDATE visit SET package = "0" WHERE package = "NOT"');
        $this->addSql('ALTER TABLE visit ADD package_new TINYINT(1) NOT NULL');
        $this->addSql('UPDATE visit SET package_new = CAST(package AS  UNSIGNED INTEGER)');
        $this->addSql('ALTER TABLE visit DROP COLUMN package');
        $this->addSql('ALTER TABLE visit CHANGE COLUMN package_new package TINYINT(1) NOT NULL');
    }
}
