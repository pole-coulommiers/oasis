<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211018143533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Person';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, title_id INT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, address_line1 VARCHAR(255) DEFAULT NULL, address_line2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postal_code VARCHAR(255) DEFAULT NULL, sender VARCHAR(255) DEFAULT NULL, notes LONGTEXT DEFAULT NULL, INDEX IDX_34DCD176A9F87BD (title_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176A9F87BD FOREIGN KEY (title_id) REFERENCES title (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE person');
    }
}
