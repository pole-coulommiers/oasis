<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211022132434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add informations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person ADD warning LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE visit ADD package TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE person DROP warning');
        $this->addSql('ALTER TABLE visit DROP package');
    }
}
