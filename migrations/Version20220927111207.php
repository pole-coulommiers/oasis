<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220927111207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add hairdressing';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE visit ADD hairdressing TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE visit DROP hairdressing');
    }
}
